package org.osmf.player.locales
{
	import org.osmf.player.utils.StrobePlayerStrings;
	
	public class JaJp extends LocaleBase
	{
		public function JaJp()
		{
		}
		
		override protected function setOsmfStrings():void
		{
			// from: https://svn.apache.org/repos/asf/flex/sdk/branches/patches/projects/spark/bundles/ja_JP/osmf.properties
			osmfResourceDict["alreadyListed"] = "この項目は既にリストに存在します";
			osmfResourceDict["alreadyLoading"] = "ローダー - 読み込み中のオブジェクトを読み込もうとしています";
			osmfResourceDict["alreadyReady"] = "ローダー - 既に読み込まれたオブジェクトを読み込もうとしています";
			osmfResourceDict["alreadyUnloaded"] = "ローダー - 既にアンロードされたオブジェクトをアンロードしようとしています";
			osmfResourceDict["alreadyUnloading"] = "ローダー - アンロード中のオブジェクトをアンロードしようとしています";
			osmfResourceDict["audioIOLoadError"] = "オーディオファイルの読み込み中に入出力エラーが発生しました";
			osmfResourceDict["audioSecurityLoadError"] = "オーディオファイルの読み込み中にセキュリティエラーが発生しました";
			osmfResourceDict["beaconFailureError"] = "ビーコンが自身の HTTP URL への ping に失敗しました";
			osmfResourceDict["compositeTraitNotFound"] = "指定された trait では、合成 trait は見つかりませんでした";
			osmfResourceDict["contentIOLoadError"] = "画像または SWF の読み込み中に入出力エラーが発生しました";
			osmfResourceDict["contentSecurityLoadError"] = "画像または SWF の読み込み中にセキュリティエラーが発生しました";
			osmfResourceDict["drmAuthenticationFailed"] = "AuthenticationFailed    SWF でユーザー認証情報の再入力とライセンス取得の再試行をユーザーに要求する必要があります";
			osmfResourceDict["drmContentNotYetValid"] = "ContentNotYetValid    取得したライセンスはまだ有効ではありません";
			osmfResourceDict["drmMetadataNotSet"] = "DRMServices でメタデータが設定されていません";
			osmfResourceDict["drmNeedsAuthentication"] = "NeedAuthentication   Remedy でユーザーを認証し、ライセンス取得を再実行します";
			osmfResourceDict["f4mParseNoId"] = "マニフェストに id タグがありません";
			osmfResourceDict["fileStructureInvalid"] = "ファイルの構造が無効です";
			osmfResourceDict["functionMustBeOverridden"] = "この関数をオーバーライドする必要があります";
			osmfResourceDict["httpIOLoadError"] = "HTTP 経由で URL を読み込む際に入出力エラーが発生しました";
			osmfResourceDict["httpSecurityLoadError"] = "HTTP 経由で URL を読み込む際にセキュリティエラーが発生しました";
			osmfResourceDict["iLoaderCantHandleResource"] = "指定された IMediaResource を ILoader で処理できません";
			osmfResourceDict["illegalConstructorInvocation"] = "クラスのインスタンスを取得するには、静的な getInstance メソッドを使用します。";
			osmfResourceDict["ioError"] = "hogehoge";
			osmfResourceDict["invalidLayoutRendererConstructor"] = "ILayoutRenderer の実装を作成できません";
			osmfResourceDict["invalidParam"] = "メソッドに無効なパラメーターが渡されました";
			osmfResourceDict["invalidPluginImplementation"] = "IPluginInfo の実装が正しくないか存在しないため、プラグインの読み込みに失敗しました";
			osmfResourceDict["invalidPluginVersion"] = "バージョン不一致のため、プラグインの読み込みに失敗しました";
			osmfResourceDict["invalidSwfASVersion"] = "AS3 より前の (AVM1) コンテンツの読み込みはサポートされていません";
			osmfResourceDict["invalidURLProtocol"] = "URL プロトコルが無効です";
			osmfResourceDict["loadedContextNonNull"] = "LoadState.UNINITIALIZED の場合、LoadedContext は null 以外の値になります";
			osmfResourceDict["loadedContextNull"] = "LoadState.READY の場合、LoadedContext は null になります";
			osmfResourceDict["missingStringResource"] = "リソース {0} のストリングがありません";
			osmfResourceDict["mustSetILoaderForLoad"] = "ILoader.load を呼び出す前に、ILoadable で ILoader を設定する必要があります";
			osmfResourceDict["mustSetILoaderForUnload"] = "ILoader.unload を呼び出す前に、ILoadable で ILoader を設定する必要があります";
			osmfResourceDict["namespaceMustBeUnique"] = "名前空間のストリングは一意である必要があります";
			osmfResourceDict["namespaceMustEqualGroupNS"] = "名前空間のストリングはグループ名前空間と一致している必要があります";
			osmfResourceDict["namespaceMustNotBeEmpty"] = "名前空間のストリングは空にできません";
			osmfResourceDict["netConnectionArgumentError"] = "NetConnection を確立する際に引数エラーが発生しました";
			osmfResourceDict["netConnectionAsyncError"] = "NetConnection を確立する際に非同期エラーが発生しました";
			osmfResourceDict["netConnectionFailed"] = "すべての NetConnection が失敗しました";
			osmfResourceDict["netConnectionIOError"] = "NetConnection を確立する際に入出力エラーが発生しました";
			osmfResourceDict["netConnectionInvalidApp"] = "無効なアプリケーションに接続しようとしています";
			osmfResourceDict["netConnectionRejected"] = "FMS サーバーにより接続が拒否されました";
			osmfResourceDict["netConnectionSecurityError"] = "NetConnection を確立する際にネットセキュリティエラーが発生しました";
			osmfResourceDict["netConnectionTimeout"] = "有効な NetConnection を確立している途中でタイムアウトしました";
			osmfResourceDict["noSupportedTrackFound"] = "サポートされているトラックが見つかりません";
			osmfResourceDict["nullNetStream"] = "trait の NetStream が null です";
			osmfResourceDict["nullParam"] = "メソッドに null のパラメーターが渡されました";
			osmfResourceDict["nullScriptPath"] = "操作では有効なスクリプトパスを設定する必要があります";
			osmfResourceDict["playFailedNetConnectionFailure"] = "NetConnection に失敗したため、再生に失敗しました";
			osmfResourceDict["playFailedNoSoundChannels"] = "使用可能なサウンドチャンネルがないため、再生に失敗しました";
			osmfResourceDict["playbackFailed"] = "再生に失敗しました";
			osmfResourceDict["streamNotFound"] = "ストリームが見つかりません";
			osmfResourceDict["streamSwitchInvalidIndex"] = "ダイナミックストリームの切り替え - 無効なインデックスが要求されました";
			osmfResourceDict["streamSwitchStreamNotFound"] = "ダイナミックストリームの切り替え - ストリームが見つかりません";
			osmfResourceDict["streamSwitchStreamNotInManualMode"] = "ダイナミックストリームの切り替え - ストリームが手動モードではありません";
			osmfResourceDict["swfIOLoadError"] = "SWF の読み込み中に入出力エラーが発生しました";
			osmfResourceDict["swfSecurityError"] = "SWF の読み込み中にセキュリティエラーが発生しました";
			osmfResourceDict["switchingDownBandwidthInsufficient"] = "平均帯域幅が現在のストリームのビットレートに対して不十分です";
			osmfResourceDict["switchingDownBufferInsufficient"] = "バッファーの長さが不十分です";
			osmfResourceDict["switchingDownFrameDropUnacceptable"] = "1 秒あたりのドロップフレーム数が現在のストリームのビットレートに対して大きすぎます";
			osmfResourceDict["switchingDownOther"] = "下方に切り替えています";
			osmfResourceDict["switchingManual"] = "手動に切り替えています";
			osmfResourceDict["switchingUpBandwidthSufficient"] = "平均帯域幅が十分にあるので上方に切り替えています";
			osmfResourceDict["switchingUpOther"] = "上方に切り替えています";
			osmfResourceDict["traitInstanceAlreadyAdded"] = "この trait クラスのインスタンスは既にこの MediaElement に追加されています";
			osmfResourceDict["traitNotSupported"] = "MediaPlayer - null のメディアまたは *trait* 以外のメディアに対してメソッドが呼び出されました";
			osmfResourceDict["traitResolverAlreadyAdded"] = "指定された trait タイプの trait リゾルバーは既にこの MediaElement に追加されています";
			osmfResourceDict["traitTypeMismatch"] = "指定された trait インスタンスは予期しないタイプです";
			osmfResourceDict["unsupportedTraitType"] = "指定された trait タイプはサポートされていません";
			
			// 
			osmfResourceDict["alreadyAdded"] = "すでに追加されています";
			osmfResourceDict["argumentError"] = "メディアの読み込み時に引数エラーが発生しました";
			osmfResourceDict["asyncError"] = "メディアの読み込み時に非同期エラーが発生しました";
			osmfResourceDict["capabilityNotSupported"] = "指定されたケーパビリティは現在サポートされていません";
			osmfResourceDict["drmSystemUpdateError"] = "DRMサブシステムの更新に失敗しました";
			osmfResourceDict["httpGetFailed"] = "HTTPのGETリクエストが失敗しました (4xxステータスコード)";
			osmfResourceDict["ioError"] = "メディアの読み込み時にI/Oエラーが発生しました";
			osmfResourceDict["mediaLoadFailed"] = "MediaElementの読み込みに失敗しました";
			osmfResourceDict["netStreamFileStructureInvalid"] = "ファイルの構造が不正です";
			osmfResourceDict["netStreamNoSupportedTrackFound"] = "サポートするトラックが見つかりませんでした";
			osmfResourceDict["netStreamPlayFailed"] = "ストリームの再生に失敗しました";
			osmfResourceDict["netStreamStreamNotFound"] = "再生するストリームが見つかりませんでした";
			osmfResourceDict["securityError"] = "メディアの読み込み時にセキュリティエラーが発生しました";
			osmfResourceDict["soundPlayFailed"] = "有効なサウンドチャンネルが無いため再生に失敗しました";
			osmfResourceDict["unsupportedMediaElementType"] = "指定されたMediaElementTypeはサポートサポートされていません";
			osmfResourceDict["urlSchemeInvalid"] = "不正なURLスキームです";
		}
		
		override protected function setStrobeMediaPlaybackStrings():void
		{
			smpResourceDict[StrobePlayerStrings.ILLEGAL_INPUT_VARIABLE]	 				= "入力パラメータが不正です";
			smpResourceDict[StrobePlayerStrings.DYNAMIC_STREAMING_RESOURCE_EXPECTED]	= "DynamicStreamingResourceはプロキシチェーンに沿って期待されていました";
			smpResourceDict[StrobePlayerStrings.UNKNOWN_ERROR]							= "不明なエラーです";
			smpResourceDict[StrobePlayerStrings.PLUGIN_NOT_IN_WHITELIST]				= "プラグインが有効リストに含まれていないため読みまれませんでした。url = {0}";
			
			smpResourceDict[StrobePlayerStrings.DEFAULT_FONT]		= "-apple-system,meiryo,メイリオ,Arial";
			
			smpResourceDict[StrobePlayerStrings.QUALITY]			= "画質";
			smpResourceDict[StrobePlayerStrings.QUALITY_AUTO]		= "自動";
			smpResourceDict[StrobePlayerStrings.QUALITY_HIGH_HD]	= "HD高画質";
			smpResourceDict[StrobePlayerStrings.QUALITY_HD]			= "HD標準画質";
			smpResourceDict[StrobePlayerStrings.QUALITY_HIGH_SD]	= "SD高画質";
			smpResourceDict[StrobePlayerStrings.QUALITY_SD]			= "SD標準画質";
		}
	}
}
