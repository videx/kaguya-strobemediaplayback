package org.osmf.player.locales
{
	import flash.utils.Dictionary;
	import org.osmf.utils.OSMFStrings;
	import org.osmf.player.utils.StrobePlayerStrings;

	public class LocaleBase
	{
		protected static var osmfResourceDict:Dictionary = new Dictionary();
		protected static var defaultOsmfResourceStringFunction:Function;
		protected static var smpResourceDict:Dictionary = new Dictionary();
		protected static var defaultSmpResourceStringFunction:Function;
		
		public function localize():void
		{
			if (!setStrobeMediaPlaybackStrings)
			{
				defaultOsmfResourceStringFunction = OSMFStrings.resourceStringFunction;
			}
			OSMFStrings.resourceStringFunction = getOsmfResourceString;
			setOsmfStrings();
			
			if (!defaultSmpResourceStringFunction)
			{
				defaultSmpResourceStringFunction = StrobePlayerStrings.resourceStringFunction;
			}
			
			StrobePlayerStrings.resourceStringFunction = getSmpResourceString;
			setStrobeMediaPlaybackStrings();
		}
		
		protected function setOsmfStrings():void
		{
			
		}
		
		public static function getOsmfResourceString(resourceName:String, params:Array=null):String
		{
			var value:String = osmfResourceDict.hasOwnProperty(resourceName) ? String(osmfResourceDict[resourceName]) : null;
			if (value != null)
			{
				return value;
			}
			return defaultOsmfResourceStringFunction(resourceName, params);
		}
		
		protected function setStrobeMediaPlaybackStrings():void
		{
			
		}
		
		public static function getSmpResourceString(resourceName:String, params:Array=null):String
		{
			var value:String = smpResourceDict.hasOwnProperty(resourceName) ? String(smpResourceDict[resourceName]) : null;
			if (value != null)
			{
				return value;
			}
			return defaultSmpResourceStringFunction(resourceName, params);
		}
	}
}
