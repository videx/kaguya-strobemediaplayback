package org.osmf.player.chrome
{
	import flash.events.Event;
	
	import org.osmf.events.MediaElementEvent;
	import org.osmf.events.MetadataEvent;
	import org.osmf.events.PlayEvent;
	import org.osmf.events.TimeEvent;
	import org.osmf.layout.HorizontalAlign;
	import org.osmf.layout.LayoutMode;
	import org.osmf.layout.VerticalAlign;
	import org.osmf.media.MediaElement;
	import org.osmf.player.chrome.assets.AssetsManager;
	import org.osmf.player.chrome.widgets.AutoHideControlWidget;
	import org.osmf.player.chrome.widgets.ScreenPlayButton;
	import org.osmf.player.chrome.widgets.ScreenStepSkipButton;
	import org.osmf.player.chrome.widgets.Widget;
	import org.osmf.player.chrome.widgets.WidgetIDs;
	import org.osmf.traits.MediaTraitType;
	import org.osmf.traits.PlayState;
	import org.osmf.traits.PlayTrait;
	import org.osmf.traits.TimeTrait;
		
	public class ScreenControlBar extends AutoHideControlWidget
	{
		public function ScreenControlBar()
		{
			super();
		}
		
		override public function configure(xml:XML, assetManager:AssetsManager):void
		{
			id = WidgetIDs.SCREEN_CONTROL_BAR;
			fadeSteps = 6;
			
			layoutMetadata.horizontalAlign = HorizontalAlign.CENTER;
			layoutMetadata.verticalAlign = VerticalAlign.MIDDLE;
			layoutMetadata.layoutMode = LayoutMode.HORIZONTAL;
			
			super.configure(xml, assetManager);
			
			leftControls = new Widget();
			leftControls.layoutMetadata.percentHeight = 100;
			leftControls.layoutMetadata.layoutMode = LayoutMode.HORIZONTAL;
			leftControls.layoutMetadata.horizontalAlign = HorizontalAlign.LEFT;
			
			_60sStepBackButton = new ScreenStepSkipButton(-60);
			_60sStepBackButton.layoutMetadata.verticalAlign = VerticalAlign.MIDDLE
			_60sStepBackButton.layoutMetadata.horizontalAlign = HorizontalAlign.LEFT;
			_60sStepBackButton.player = this.player;
			leftControls.addChildWidget(_60sStepBackButton);
			
			_10sStepBackButton = new ScreenStepSkipButton(-10);
			_10sStepBackButton.layoutMetadata.verticalAlign = VerticalAlign.MIDDLE
			_10sStepBackButton.layoutMetadata.horizontalAlign = HorizontalAlign.LEFT;
			_10sStepBackButton.player = this.player;
			leftControls.addChildWidget(_10sStepBackButton);
			
			var screenPlayButton:ScreenPlayButton = new ScreenPlayButton();
			screenPlayButton.layoutMetadata.verticalAlign = VerticalAlign.MIDDLE
			screenPlayButton.layoutMetadata.horizontalAlign = HorizontalAlign.CENTER;
			screenPlayButton.player = this.player;
			
			rightControls = new Widget();
			rightControls.layoutMetadata.percentHeight = 100;
			rightControls.layoutMetadata.layoutMode = LayoutMode.HORIZONTAL;
			rightControls.layoutMetadata.horizontalAlign = HorizontalAlign.LEFT;
			
			_10sStepForwardButton = new ScreenStepSkipButton(10);
			_10sStepForwardButton.layoutMetadata.verticalAlign = VerticalAlign.MIDDLE
			_10sStepForwardButton.layoutMetadata.horizontalAlign = HorizontalAlign.LEFT;
			_10sStepForwardButton.player = this.player;
			rightControls.addChildWidget(_10sStepForwardButton);
			
			_60sStepForwardButton = new ScreenStepSkipButton(60);
			_60sStepForwardButton.player = this.player;			
			rightControls.addChildWidget(_60sStepForwardButton);
			
			leftControls.visible = false;
			rightControls.visible = false;
			
			addChildWidget(leftControls);
			addChildWidget(screenPlayButton);
			addChildWidget(rightControls);
			
			configureWidgets([leftControls, _60sStepBackButton, _10sStepBackButton,
				screenPlayButton,
				rightControls, _10sStepForwardButton, _60sStepForwardButton]);

			measure();
		}
		
		private function configureWidgets(widgets:Array):void
		{
			for each( var widget:Widget in widgets)
			{
				if (widget)
				{
					widget.configure(<default/>, assetManager);
				}
			}
		}
		
		private function onMediaStarted():void
		{
			if (!_mediaStarted) {
				_mediaStarted = true;
				refreshWidgets();
				visible = true
				autoHide = true;
			}
		}
		
		private function refreshWidgets():void
		{
			if (!_timeTrait) {
				return;
			}
			
			if (!_mediaStarted) {
				return;
			}
			
			if (_stepButtonStatus == "auto") {
				leftControls.visible = (_timeTrait.duration > _minDurationForStepButton);
				rightControls.visible = (_timeTrait.duration > _minDurationForStepButton);
			} else {
				leftControls.visible = _stepButtonEnabled;
				rightControls.visible = _stepButtonEnabled;
			}
			
			var stageWidth:int = this.stage != null ? this.stage.stageWidth : NaN;
			_10sStepBackButton.visible = (stageWidth >= 280);
			_10sStepForwardButton.visible = (stageWidth >= 280);
			_60sStepBackButton.visible = (stageWidth >= 460);
			_60sStepForwardButton.visible = (stageWidth >= 460);
		}
		
		private function onPlayStateChange(event:Event = null):void
		{
			if (!_playable) return;
						
			if (_playable.playState == PlayState.STOPPED)
			{
				autoHide = false;
				visible = false;
			} else {
				visible = true
				autoHide = true;
			}
		}
		
		private function onDurationChange(event:Event = null):void
		{
			onMediaStarted();
			refreshWidgets();
		}
		
		override public function layout(availableWidth:Number, availableHeight:Number, deep:Boolean=true):void
		{
			super.layout(availableWidth, availableHeight, deep);
			refreshWidgets();
		}
		
		override protected function get requiredTraits():Vector.<String>
		{
			return _requiredTraits;
		}
		
		override protected function processRequiredTraitsAvailable(element:MediaElement):void
		{
			setPlayable(element.getTrait(MediaTraitType.PLAY) as PlayTrait);
			setTimeTrait(element.getTrait(MediaTraitType.TIME) as TimeTrait);
		}
		
		override protected function processRequiredTraitsUnavailable(element:MediaElement):void
		{
			setPlayable(null);
			setTimeTrait(null);
		}
		
		override public function set media(value:MediaElement):void
		{
			super.media = value;
			
			_mediaStarted = false;
			autoHide = false;
			visible = false;
		}
		
		private function setPlayable(value:PlayTrait):void
		{
			if (value != _playable)
			{
				if (_playable != null)
				{
					_playable.removeEventListener(PlayEvent.PLAY_STATE_CHANGE, onPlayStateChange);
					_playable = null;
				}
				
				_playable = value;
				
				if (_playable)
				{
					_playable.addEventListener(PlayEvent.PLAY_STATE_CHANGE, onPlayStateChange);
				}
			}
		}
		
		private function setTimeTrait(value:TimeTrait):void
		{
			if (value != _timeTrait)
			{
				if (_timeTrait != null)
				{
					_timeTrait.removeEventListener(TimeEvent.DURATION_CHANGE, onDurationChange);
					_timeTrait = null;
				}
				
				_timeTrait = value;
				
				if (_timeTrait)
				{
					_timeTrait.addEventListener(TimeEvent.DURATION_CHANGE, onDurationChange);
				}
			}
		}
		
		public function set stepButtonEnabled(value:Boolean):void
		{
			if (_stepButtonEnabled != value) {
				_stepButtonEnabled = value;
				refreshWidgets();
			}
			_stepButtonStatus = "manual";
		}
		
		public function get isStepButtonEnabled():Boolean
		{
			return _stepButtonEnabled;
		}
		
		/* static */
		private static const _requiredTraits:Vector.<String> = new Vector.<String>;
		_requiredTraits[0] = MediaTraitType.PLAY;
		_requiredTraits[1] = MediaTraitType.TIME;
		
		private var _mediaStarted:Boolean = false;
		private var _playable:PlayTrait;
		private var _timeTrait:TimeTrait;
		
		private var leftControls:Widget;
		private var rightControls:Widget;
		
		private var _minDurationForStepButton:int = 300;
		private var _stepButtonStatus:String = "auto";
		private var _stepButtonEnabled:Boolean = true;
		private var _10sStepBackButton:ScreenStepSkipButton;
		private var _60sStepBackButton:ScreenStepSkipButton;
		private var _10sStepForwardButton:ScreenStepSkipButton;
		private var _60sStepForwardButton:ScreenStepSkipButton;
	}
}
