package org.osmf.player.chrome
{
	import flash.display.Stage;
	import flash.display.StageDisplayState;
	import flash.events.EventDispatcher;
	import flash.events.KeyboardEvent;
	import flash.ui.Keyboard;
	
	import org.osmf.media.MediaElement;
	import org.osmf.player.chrome.events.WidgetEvent;
	import org.osmf.player.media.StrobeMediaPlayer;
	import org.osmf.traits.AudioTrait;
	import org.osmf.traits.MediaTraitType;
	import org.osmf.traits.PlayState;
	import org.osmf.traits.PlayTrait;
	import org.osmf.traits.SeekTrait;
	import org.osmf.traits.TimeTrait;

	public class KeyboardShortcut extends EventDispatcher
	{
		public function KeyboardShortcut(stage:Stage)
		{
			super();
			
			_stage = stage;
			_stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
		}
		
		private function onKeyDown(event:KeyboardEvent):void
		{
			switch (event.keyCode) {
				// play or pause
				case Keyboard.SPACE:
					togglePlay();
					break;
				case Keyboard.P:
					if (event.ctrlKey) {
						togglePlay();
					}
					break;
				case Keyboard.S:
					var playTrait:PlayTrait = media ? media.getTrait(MediaTraitType.PLAY) as PlayTrait : null;
					if (event.ctrlKey && playTrait && playTrait.playState == PlayState.PLAYING) {
						togglePlay();
					}
					break;
				
				// fullscreen
				case Keyboard.ENTER:
					if (event.ctrlKey && _stage.displayState == StageDisplayState.NORMAL) {
						toggleFullscreen();
					}
					break;
				
				// close
				case Keyboard.C:
					if (event.ctrlKey) {
						dispatchEvent(new WidgetEvent(WidgetEvent.REQUEST_CLOSE));
					}
					break;
				
				// seek
				case Keyboard.LEFT:
				case Keyboard.RIGHT:
					var skipStep:Number = event.ctrlKey ? SKIP_STEP : SHORT_SKIP_STEP;
					if (event.keyCode == Keyboard.LEFT) {
						skipStep *= -1;
					}
					var timeTrait:TimeTrait = media ? media.getTrait(MediaTraitType.TIME) as TimeTrait : null;
					var seekTrait:SeekTrait = media ? media.getTrait(MediaTraitType.SEEK) as SeekTrait : null;
					if (timeTrait && seekTrait)
					{
						var time:Number = timeTrait.currentTime + skipStep;
						if (seekTrait.canSeekTo(time)) 
						{
							seekTrait.seek(time);
						}
					}
					break;
				
				// volume
				case Keyboard.M:
				case Keyboard.F7:
					toggleMute();
					break;
				case Keyboard.F8:
				case Keyboard.DOWN:
					doVolumeDown()
					break;
				case Keyboard.F9:
				case Keyboard.UP:
					doVolumeUp();
					break;
			}
		}
		
		private function doVolumeUp():void
		{
			var audioTrait:AudioTrait = media ? media.getTrait(MediaTraitType.AUDIO) as AudioTrait : null;
			if (audioTrait) {
				audioTrait.volume += VOLUME_STEP;
			}
		}
		
		private function doVolumeDown():void
		{
			var audioTrait:AudioTrait = media ? media.getTrait(MediaTraitType.AUDIO) as AudioTrait : null;
			if (audioTrait) {
				audioTrait.volume -= VOLUME_STEP;
			}
		}
		
		private function togglePlay():void
		{
			player.playOrPause();
		}
		
		private function toggleMute():void
		{
			var audioTrait:AudioTrait = media ? media.getTrait(MediaTraitType.AUDIO) as AudioTrait : null;
			if (audioTrait) {
				audioTrait.muted = !audioTrait.muted;
			}
		}
		
		private function toggleFullscreen():void
		{
			if (_stage.displayState != StageDisplayState.NORMAL) {
				_stage.displayState = StageDisplayState.NORMAL;
			} else {
				dispatchEvent(new WidgetEvent(WidgetEvent.REQUEST_FULL_SCREEN));
			}
		}
		
		public function set player(value:StrobeMediaPlayer):void
		{
			if (_player != value)
			{
				_player = value;
			}
		}
		
		public function get player():StrobeMediaPlayer
		{
			return _player;
		}
		
		public function set media(value:MediaElement):void
		{
			if (_media != value) {
				_media = value;
			}
		}
		
		public function get media():MediaElement
		{
			return _media;
		}
		
		private const SHORT_SKIP_STEP:Number = 10;
		private const SKIP_STEP:Number = 60;
		private const VOLUME_STEP:Number = 0.1;
		
		private var _stage:Stage;
		private var _player:StrobeMediaPlayer;
		private var _media:MediaElement;
	}
}