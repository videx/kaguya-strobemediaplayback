package org.osmf.player.chrome
{
	import flash.events.Event;
	import flash.events.FullScreenEvent;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	
	import org.osmf.player.chrome.assets.AssetsManager;
	import org.osmf.player.chrome.widgets.WidgetIDs;
	import org.osmf.player.utils.StrobePlayerStrings;
	import org.osmf.player.chrome.widgets.AutoHideControlWidget;
	
	public class TitleBar extends AutoHideControlWidget
	{
		public function TitleBar(title:String)
		{
			this.text = title ? title : "";
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			super();
		}
		
		override public function configure(xml:XML, assetManager:AssetsManager):void
		{
			id = WidgetIDs.TITLEL_BAR;
			
			layoutMetadata.height = DEFAULT_TITLE_HEIGHT + 2;
			layoutMetadata.percentWidth = 100;
			
			var textFormat:TextFormat = new TextFormat();
			textFormat.size = 12;
			textFormat.font = StrobePlayerStrings.getString(StrobePlayerStrings.DEFAULT_FONT);
			textFormat.align = TextFieldAutoSize.LEFT;
			textFormat.leftMargin = 10;
			
			textLabel = new TextField();
			textLabel.defaultTextFormat = textFormat;
			textLabel.multiline = true;
			textLabel.mouseEnabled = false;
			textLabel.textColor = 0xffffff;
			textLabel.wordWrap = true;
			textLabel.height = DEFAULT_TITLE_HEIGHT;
			textLabel.htmlText = _text;
			
			addChild(textLabel);
			textLabel.y = 3;
			
			super.configure(xml, assetManager);	
			
			this.visible = false;
			measure();
		}
		
		override public function layout(availableWidth:Number, availableHeight:Number, deep:Boolean=true):void
		{
			this.textLabel.width = availableWidth;
			this.graphics.clear();
			this.graphics.beginFill(0x000000, 0.6);
			this.graphics.drawRect(0, 0, availableWidth, availableHeight);
			this.graphics.endFill();
			
			super.layout(availableWidth, availableHeight, deep);
		}
		
		private function onAddedToStage(event:Event):void
		{
			stage.addEventListener(FullScreenEvent.FULL_SCREEN, onFullScreenEvent);
		}
		
		private function onFullScreenEvent(event:FullScreenEvent):void
		{
			fullScreenState = event.type;
			if (event.fullScreen && _text && _text.length > 0) {
				this.visible = true;
				this.autoHide = true;
			} else {
				this.autoHide = false;
				this.visible = false;
			}
		}
		
		public function set text(value:String):void
		{
			if (textLabel != null) {
				textLabel.htmlText = value;
			}
			_text = value;
		}
		
		public static const DEFAULT_TITLE_HEIGHT:Number = 28;
		
		private var _text:String;
		private var textLabel:TextField;
		private var fullScreenState:String = "";
	}
}
