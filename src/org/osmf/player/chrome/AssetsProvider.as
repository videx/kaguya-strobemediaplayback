/***********************************************************
 * Copyright 2010 Adobe Systems Incorporated.  All Rights Reserved.
 *
 * *********************************************************
 * The contents of this file are subject to the Berkeley Software Distribution (BSD) Licence
 * (the "License"); you may not use this file except in
 * compliance with the License. 
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations
 * under the License.
 *
 *
 * The Initial Developer of the Original Code is Adobe Systems Incorporated.
 * Portions created by Adobe Systems Incorporated are Copyright (C) 2010 Adobe Systems
 * Incorporated. All Rights Reserved.
 * 
 **********************************************************/

package org.osmf.player.chrome
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.utils.getQualifiedClassName;
	
	import org.osmf.player.chrome.assets.AssetIDs;
	import org.osmf.player.chrome.assets.AssetLoader;
	import org.osmf.player.chrome.assets.AssetsManager;
	import org.osmf.player.chrome.assets.BitmapResource;
	import org.osmf.player.chrome.assets.FontResource;
	import org.osmf.player.chrome.assets.SymbolResource;

	[Event(name="complete", type="flash.events.Event")]
	
	/**
	 * AssetsProvider provides the player's default chrome assets.
	 */	
	public class AssetsProvider extends EventDispatcher
	{
		// Public interface
		//
		
		public function AssetsProvider(assetsManager:AssetsManager = null)
		{
			_assetsManager = assetsManager || new AssetsManager();
			addDefaultAssets();
		}
		
		public function load():void
		{
			_assetsManager.addEventListener(Event.COMPLETE, onAssetsManagerComplete);
			_assetsManager.load();
		}
		
		public function get assetsManager():AssetsManager
		{
			return _assetsManager;
		}
		
		// Internals
		//
		
		private function addEmbeddedBitmap(id:String, symbolClass:Class):void
		{	
			var resource:BitmapResource
				= new BitmapResource
					( id
					, getQualifiedClassName(symbolClass)
					, true
					, null 
					);
			_assetsManager.addAsset(resource, new AssetLoader());
		}
		
		private function addEmbeddedSymbol(id:String, symbolClass:Class):void
		{
			
			var resource:SymbolResource
				= new SymbolResource
					( id
					, getQualifiedClassName(symbolClass)
					, true
					, null
					);
			_assetsManager.addAsset(resource, new AssetLoader());
		}
		
		private function addDefaultAssets():void
		{				
			// Default font:
			_assetsManager.addAsset
				( new FontResource
					( AssetIDs.DEFAULT_FONT
					, getQualifiedClassName(ASSET_DefaultFont)
					, true
					, AssetIDs.DEFAULT_FONT
					, 12
					, 0xDDDDDD
					)
				, new AssetLoader()
				);
			
			_assetsManager.addAsset
				( new FontResource
					( AssetIDs.DEFAULT_FONT_BOLD
						, getQualifiedClassName(ASSET_DefaultFontBold)
						, true
						, AssetIDs.DEFAULT_FONT_BOLD
						, 12
						, 0xDDDDDD
						, true
					)
					, new AssetLoader()
				);
			
			addEmbeddedSymbol(AssetIDs.CONTROL_BAR_BACKDROP, ASSET_backDrop_center);
			addEmbeddedSymbol(AssetIDs.CONTROL_BAR_BACKDROP_LEFT, ASSET_backDrop_left);
			addEmbeddedSymbol(AssetIDs.CONTROL_BAR_BACKDROP_RIGHT, ASSET_backDrop_right);
			
			// Scrub bar:
			addEmbeddedSymbol(AssetIDs.SCRUB_BAR_TRACK, ASSET2_scrub_no_load);
			addEmbeddedSymbol(AssetIDs.SCRUB_BAR_TRACK_LEFT, ASSET2_scrub_left);
			addEmbeddedSymbol(AssetIDs.SCRUB_BAR_TRACK_RIGHT, ASSET2_scrub_right);
			addEmbeddedSymbol(AssetIDs.SCRUB_BAR_LOADED_TRACK, ASSET2_scrub_loaded);
			addEmbeddedSymbol(AssetIDs.SCRUB_BAR_LOADED_TRACK_END, ASSET2_scrub_loaded_end);
			addEmbeddedSymbol(AssetIDs.SCRUB_BAR_PLAYED_TRACK, ASSET2_scrub_loaded_played);
			addEmbeddedSymbol(AssetIDs.SCRUB_BAR_PLAYED_TRACK_SEEKING, ASSET_scrub_loaded_played_seeking);
			
			addEmbeddedSymbol(AssetIDs.SCRUB_BAR_DVR_LIVE_TRACK, ASSET_ScrubDvrLive);
			addEmbeddedSymbol(AssetIDs.SCRUB_BAR_DVR_LIVE_INACTIVE_TRACK, ASSET_ScrubDvrLiveInactive);	
			addEmbeddedSymbol(AssetIDs.SCRUB_BAR_LIVE_ONLY_TRACK, ASSET_ScrubLive);
			addEmbeddedSymbol(AssetIDs.SCRUB_BAR_LIVE_ONLY_INACTIVE_TRACK, ASSET_ScrubLiveInactive);
			
			addEmbeddedSymbol(AssetIDs.SCRUB_BAR_SCRUBBER_NORMAL, ASSET2_scrub_tab);
			addEmbeddedSymbol(AssetIDs.SCRUB_BAR_SCRUBBER_DOWN, ASSET2_scrub_tab);
			addEmbeddedSymbol(AssetIDs.SCRUB_BAR_SCRUBBER_OVER, ASSET2_scrub_tab);
			addEmbeddedSymbol(AssetIDs.SCRUB_BAR_SCRUBBER_OVER, ASSET2_scrub_tab);
			
			addEmbeddedSymbol(AssetIDs.SCRUB_BAR_TIME_HINT, ASSET_time_hint);
						
			// Play button:
			addEmbeddedSymbol(AssetIDs.PLAY_BUTTON_NORMAL, ASSET2_play_normal);
			addEmbeddedSymbol(AssetIDs.PLAY_BUTTON_DOWN, ASSET2_play_normal);
			addEmbeddedSymbol(AssetIDs.PLAY_BUTTON_OVER, ASSET2_play_over);
			
			// Play overlay:
			addEmbeddedSymbol(AssetIDs.PLAY_BUTTON_OVERLAY_NORMAL, ASSET2_play_overlayed);
			addEmbeddedSymbol(AssetIDs.PLAY_BUTTON_OVERLAY_DOWN, ASSET2_play_overlayed);
			addEmbeddedSymbol(AssetIDs.PLAY_BUTTON_OVERLAY_OVER, ASSET2_play_overlayed);
			
			// Pause button:
			addEmbeddedSymbol(AssetIDs.PAUSE_BUTTON_NORMAL, ASSET2_pause_normal);
			addEmbeddedSymbol(AssetIDs.PAUSE_BUTTON_DOWN, ASSET2_pause_over);
			addEmbeddedSymbol(AssetIDs.PAUSE_BUTTON_OVER, ASSET2_pause_over);
			
			// Mute:
			addEmbeddedSymbol(AssetIDs.VOLUME_BUTTON_NORMAL, ASSET2_volume_med_normal);
			addEmbeddedSymbol(AssetIDs.VOLUME_BUTTON_DOWN, ASSET2_volume_med_over);
			addEmbeddedSymbol(AssetIDs.VOLUME_BUTTON_OVER, ASSET2_volume_med_over);

			addEmbeddedSymbol(AssetIDs.VOLUME_BUTTON_LOW_NORMAL, ASSET2_volume_med_normal);
			addEmbeddedSymbol(AssetIDs.VOLUME_BUTTON_LOW_DOWN, ASSET2_volume_med_over);
			addEmbeddedSymbol(AssetIDs.VOLUME_BUTTON_LOW_OVER, ASSET2_volume_med_over);

			addEmbeddedSymbol(AssetIDs.VOLUME_BUTTON_MED_NORMAL, ASSET2_volume_med_normal);
			addEmbeddedSymbol(AssetIDs.VOLUME_BUTTON_MED_DOWN, ASSET2_volume_med_over);
			addEmbeddedSymbol(AssetIDs.VOLUME_BUTTON_MED_OVER, ASSET2_volume_med_over);

			addEmbeddedSymbol(AssetIDs.VOLUME_BUTTON_HIGH_NORMAL, ASSET2_volume_high_normal);
			addEmbeddedSymbol(AssetIDs.VOLUME_BUTTON_HIGH_DOWN, ASSET2_volume_high_over);
			addEmbeddedSymbol(AssetIDs.VOLUME_BUTTON_HIGH_OVER, ASSET2_volume_high_over);

			// Unmute:
			addEmbeddedSymbol(AssetIDs.UNMUTE_BUTTON_NORMAL, ASSET2_volume_mute_normal);
			addEmbeddedSymbol(AssetIDs.UNMUTE_BUTTON_DOWN, ASSET2_volume_mute_over);
			addEmbeddedSymbol(AssetIDs.UNMUTE_BUTTON_OVER, ASSET2_volume_mute_over);
			
			// Volume slider:
			addEmbeddedSymbol(AssetIDs.VOLUME_BAR_BACKDROP, ASSET_volume_back);
			addEmbeddedSymbol(AssetIDs.VOLUME_BAR_TRACK, ASSET2_volume_scrub);
			addEmbeddedSymbol(AssetIDs.VOLUME_BAR_TRACK_END, ASSET2_volume_scrub_bottom);
			addEmbeddedSymbol(AssetIDs.VOLUME_BAR_SLIDER_NORMAL, ASSET_volume_slider);
			addEmbeddedSymbol(AssetIDs.VOLUME_BAR_SLIDER_DOWN, ASSET_volume_slider);
			addEmbeddedSymbol(AssetIDs.VOLUME_BAR_SLIDER_OVER, ASSET_volume_slider);
			
			// Fullscreen enter:
			addEmbeddedSymbol(AssetIDs.FULL_SCREEN_ENTER_NORMAL, ASSET2_fullscreen_on_normal);
			addEmbeddedSymbol(AssetIDs.FULL_SCREEN_ENTER_DOWN, ASSET2_fullscreen_on_over);
			addEmbeddedSymbol(AssetIDs.FULL_SCREEN_ENTER_OVER, ASSET2_fullscreen_on_over);
			
			// Fullscreen leave:
			addEmbeddedSymbol(AssetIDs.FULL_SCREEN_LEAVE_NORMAL, ASSET2_fullscreen_off_normal);
			addEmbeddedSymbol(AssetIDs.FULL_SCREEN_LEAVE_DOWN, ASSET2_fullscreen_off_over);
			addEmbeddedSymbol(AssetIDs.FULL_SCREEN_LEAVE_OVER, ASSET2_fullscreen_off_over);
			
			// close button
			addEmbeddedSymbol(AssetIDs.CLOSE_BUTTON_NORMAL, ASSET2_alt_close_normal);
			addEmbeddedSymbol(AssetIDs.CLOSE_BUTTON_DOWN, ASSET2_alt_close_over);
			addEmbeddedSymbol(AssetIDs.CLOSE_BUTTON_OVER, ASSET2_alt_close_over);
			
			// screen button
			addEmbeddedSymbol(AssetIDs.SCREEN_PAUSE_BUTTON_NORMAL, ASSET2_big_pause);
			addEmbeddedSymbol(AssetIDs.SCREEN_PLAY_BUTTON_NORMAL, ASSET2_big_play);
			addEmbeddedSymbol(AssetIDs.SCREEN_10_STEP_BACK_BUTTON_NORMAL, ASSET2_10s_step_back);
			addEmbeddedSymbol(AssetIDs.SCREEN_60_STEP_BACK_BUTTON_NORMAL, ASSET2_60s_step_back);
			addEmbeddedSymbol(AssetIDs.SCREEN_10_STEP_FORWARD_BUTTON_NORMAL, ASSET2_10s_step_forward);
			addEmbeddedSymbol(AssetIDs.SCREEN_60_STEP_FORWARD_BUTTON_NORMAL, ASSET2_60s_step_forward);
			
			// Authentication dialog:
			addEmbeddedSymbol(AssetIDs.AUTH_BACKDROP, ASSET_auth_backdrop);
			addEmbeddedSymbol(AssetIDs.AUTH_SUBMIT_BUTTON_NORMAL, ASSET_button_normal);
			addEmbeddedSymbol(AssetIDs.AUTH_SUBMIT_BUTTON_DOWN, ASSET_button_selected);
			addEmbeddedSymbol(AssetIDs.AUTH_SUBMIT_BUTTON_OVER, ASSET_button_over);
			addEmbeddedSymbol(AssetIDs.AUTH_CANCEL_BUTTON_NORMAL, ASSET_close_normal);
			addEmbeddedSymbol(AssetIDs.AUTH_CANCEL_BUTTON_OVER, ASSET_close_over);
			addEmbeddedSymbol(AssetIDs.AUTH_CANCEL_BUTTON_DOWN, ASSET_close_selected);
			addEmbeddedSymbol(AssetIDs.AUTH_WARNING, ASSET_warning);
			
			// Previous button:
			addEmbeddedSymbol(AssetIDs.PREVIOUS_BUTTON_NORMAL, ASSET_previous_normal);
			addEmbeddedSymbol(AssetIDs.PREVIOUS_BUTTON_DOWN, ASSET_previous_selected);
			addEmbeddedSymbol(AssetIDs.PREVIOUS_BUTTON_OVER, ASSET_previous_rollover);
			addEmbeddedSymbol(AssetIDs.PREVIOUS_BUTTON_DISABLED, ASSET_previous_disabled)
			
			// Next button:
			addEmbeddedSymbol(AssetIDs.NEXT_BUTTON_NORMAL, ASSET_next_normal);
			addEmbeddedSymbol(AssetIDs.NEXT_BUTTON_DOWN, ASSET_next_selected);
			addEmbeddedSymbol(AssetIDs.NEXT_BUTTON_OVER, ASSET_next_rollover);
			addEmbeddedSymbol(AssetIDs.NEXT_BUTTON_DISABLED, ASSET_next_disabled)
			
			// HD indicator:
			addEmbeddedSymbol(AssetIDs.HD_ON, ASSET_hd_on);
			addEmbeddedSymbol(AssetIDs.HD_OFF, ASSET_hd_off);
			
			// SD,HD,Auto
			addEmbeddedSymbol(AssetIDs.QUALITY_AUTO, ASSET2_auto);
			addEmbeddedSymbol(AssetIDs.QUALITY_HD_HIGH, ASSET2_hd_high);
			addEmbeddedSymbol(AssetIDs.QUALITY_HD_LOW, ASSET2_hd_low);
			addEmbeddedSymbol(AssetIDs.QUALITY_SD_HIGH, ASSET2_sd_high);
			addEmbeddedSymbol(AssetIDs.QUALITY_SD_LOW, ASSET2_sd_low);
			
			// Buffering overlay:
			addEmbeddedSymbol(AssetIDs.BUFFERING_OVERLAY, ASSET3_loading_icon);
		}
		
		private function onAssetsManagerComplete(event:Event):void
		{
			// Redispatch the completion event:
			dispatchEvent(event.clone());	
		}
		
		private var _assetsManager:AssetsManager;
	}
}