package org.osmf.player.chrome
{
	import flash.events.Event;
	import flash.events.FullScreenEvent;
	
	import org.osmf.player.chrome.ChromeProvider;
	import org.osmf.player.chrome.assets.AssetsManager;
	import org.osmf.player.chrome.widgets.AutoHideControlWidget;
	import org.osmf.player.chrome.widgets.WidgetIDs;
	import org.osmf.player.chrome.widgets.WindowCloseButton;
	
	public class WindowControlBar extends AutoHideControlWidget
	{
		public function WindowControlBar()
		{
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			super();
		}
		
		override public function configure(xml:XML, assetManager:AssetsManager):void
		{
			id = WidgetIDs.WINDOW_CONTROL_BAR;
			fadeSteps = 6;
			
			super.configure(xml, assetManager);
			
			// button
			_closeButton = new WindowCloseButton();
			_closeButton.configure(<default/>, ChromeProvider.getInstance().assetManager);
			_closeButton.visible = true;
			addChildWidget(closeButton);
			
			measure();
		}
		
		private function onAddedToStage(event:Event):void
		{
			stage.addEventListener(FullScreenEvent.FULL_SCREEN, onFullScreenEvent);
		}
		
		private function onFullScreenEvent(event:FullScreenEvent):void
		{
			// hide if fullscreen
			if (event.fullScreen) {
				setSuperVisible(false);
			} else {
				setSuperVisible(true);
			}
		}
		
		public function get closeButton():WindowCloseButton
		{
			return _closeButton;
		}
		
		private var _closeButton:WindowCloseButton;
	}
}
