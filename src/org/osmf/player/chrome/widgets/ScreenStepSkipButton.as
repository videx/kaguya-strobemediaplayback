package org.osmf.player.chrome.widgets
{
	import flash.display.DisplayObject;
	import flash.events.MouseEvent;
	
	import org.osmf.media.MediaElement;
	import org.osmf.player.chrome.assets.AssetIDs;
	import org.osmf.traits.MediaTraitType;
	import org.osmf.traits.SeekTrait;
	import org.osmf.traits.TimeTrait;
	
	public class ScreenStepSkipButton extends ButtonWidget
	{
		public function ScreenStepSkipButton(step)
		{
			super();
			
			_step = step;
			
			switch (step)
			{
				case -10:
					upFace = downFace = overFace = AssetIDs.SCREEN_10_STEP_BACK_BUTTON_NORMAL;
					break;
				case -60:
					upFace = downFace = overFace = AssetIDs.SCREEN_60_STEP_BACK_BUTTON_NORMAL;
					break;
				case 10:
					upFace = downFace = overFace = AssetIDs.SCREEN_10_STEP_FORWARD_BUTTON_NORMAL;
					break;
				case 60:
					upFace = downFace = overFace = AssetIDs.SCREEN_60_STEP_FORWARD_BUTTON_NORMAL;
					break;
			}
		}
		
		// Overrides
		//
		
		override protected function onMouseClick(event:MouseEvent):void
		{
			var timeTrait:TimeTrait = media ? media.getTrait(MediaTraitType.TIME) as TimeTrait : null;
			var seekTrait:SeekTrait = media ? media.getTrait(MediaTraitType.SEEK) as SeekTrait : null;
			if (timeTrait && seekTrait)
			{
				var time:Number = timeTrait.currentTime + _step;
				if (seekTrait.canSeekTo(time)) 
				{
					seekTrait.seek(time);
				}
			}
			event.stopImmediatePropagation();
		}
		
		override protected function onMouseDown(event:MouseEvent):void
		{
			// keep mouseOver
			mouseOver = true;
			setFace(enabled ? down : disabled);
		}
		
		override protected function get requiredTraits():Vector.<String>
		{
			return _requiredTraits;
		}
		
		override protected function processRequiredTraitsAvailable(media:MediaElement):void
		{
		}
		
		override protected function processRequiredTraitsUnavailable(media:MediaElement):void
		{
		}
		
		override protected function setFace(face:DisplayObject):void
		{
			super.setFace(face);
			if (currentFace) {
				currentFace.alpha = mouseOver ? 1.0 : 0.2;
			}
		}
		
		private var _step:Number;
		private static const _requiredTraits:Vector.<String> = new Vector.<String>;
		_requiredTraits[0] = MediaTraitType.TIME;
		_requiredTraits[1] = MediaTraitType.SEEK;
	}
}