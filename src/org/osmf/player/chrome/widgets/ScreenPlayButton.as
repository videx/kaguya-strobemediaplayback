package org.osmf.player.chrome.widgets
{
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import org.osmf.player.chrome.assets.AssetIDs;
	import org.osmf.traits.MediaTraitType;
	import org.osmf.traits.PlayState;
	import org.osmf.traits.PlayTrait;
	
	public class ScreenPlayButton extends PlayableButton
	{
		public var steppedButtonFaces:Array =[
			{ 
				up: AssetIDs.SCREEN_PAUSE_BUTTON_NORMAL,
				down: AssetIDs.SCREEN_PAUSE_BUTTON_NORMAL,
				over: AssetIDs.SCREEN_PAUSE_BUTTON_NORMAL
			},
			{ 
				up: AssetIDs.SCREEN_PLAY_BUTTON_NORMAL,
				down: AssetIDs.SCREEN_PLAY_BUTTON_NORMAL,
				over: AssetIDs.SCREEN_PLAY_BUTTON_NORMAL
			}
		];
		
		public function ScreenPlayButton()
		{
			super();
		}
		
		// Overrides
		//
		
		override protected function setFace(face:DisplayObject):void
		{
			super.setFace(face);
			if (currentFace) {
				currentFace.alpha = mouseOver ? 1.0 : 0.2;
			}
		}
		
		override protected function onMouseDown(event:MouseEvent):void
		{
			// keep mouseOver
			mouseOver = true;
			setFace(enabled ? down : disabled);
		}
		
		override protected function onMouseClick(event:MouseEvent):void
		{
			var playable:PlayTrait = media.getTrait(MediaTraitType.PLAY) as PlayTrait;
			if (playable.playState == PlayState.PLAYING)
			{
				if ( playable.canPause)
				{
					playable.pause();
				}
				else
				{
					playable.stop();
				}
			} else {
				playable.play();
			}
			event.stopImmediatePropagation();
		}
		
		override protected function visibilityDeterminingEventHandler(event:Event = null):void
		{
			var currentFaceIndex:Number = (playable && playable.playState == PlayState.PLAYING) ? 0 : 1;
			
			up = assetManager.getDisplayObject(steppedButtonFaces[currentFaceIndex].up);
			down = assetManager.getDisplayObject(steppedButtonFaces[currentFaceIndex].down);
			over = assetManager.getDisplayObject(steppedButtonFaces[currentFaceIndex].over);
			
			setFace(up);
		}
	}
}