package org.osmf.player.chrome.widgets
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.AntiAliasType;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	
	import org.osmf.events.DynamicStreamEvent;
	import org.osmf.media.MediaElement;
	import org.osmf.media.MediaPlayer;
	import org.osmf.net.DynamicStreamingItem;
	import org.osmf.player.chrome.assets.AssetsManager;
	import org.osmf.player.chrome.assets.FontAsset;
	import org.osmf.player.metadata.MediaMetadata;
	import org.osmf.player.metadata.ResourceMetadata;
	import org.osmf.player.utils.StrobePlayerStrings;
	import org.osmf.traits.DynamicStreamTrait;
	import org.osmf.traits.MediaTraitType;
	
	public class QualityMenu extends Widget
	{
		public function QualityMenu()
		{
			super();
			mouseEnabled = true;
			
			addEventListener(MouseEvent.CLICK, onMouseClick);
			addEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
			addEventListener(MouseEvent.MOUSE_OVER, onMouseOver);

			clickArea = new Sprite();
			addChild(clickArea);
		}
		
		override public function configure(xml:XML, assetManager:AssetsManager):void
		{
			super.configure(xml, assetManager);
			
			// apply config
			if (QualityMenu.menuConfig) {
				try {
					var conf:Object = JSON.parse(QualityMenu.menuConfig);
					if (conf) {
						if (conf.hasOwnProperty('text_color')) {
							textColor = conf.text_color;
						}
						if (conf.hasOwnProperty('background_color')) {
							backgroundColor = conf.background_color;
						}
						if (conf.hasOwnProperty('background_opacity')) {
							backgroundOpacity = conf.background_opacity;
						}
						if (conf.hasOwnProperty('border_color')) {
							borderColor = conf.border_color;
						}
						if (conf.hasOwnProperty('border_opacity')) {
							borderOpacity = conf.border_opacity;
						}
						if (conf.hasOwnProperty('selected_color')) {
							selectedColor = conf.selected_color;
						}
						if (conf.hasOwnProperty('selected_opacity')) {
							selectedOpacity = conf.selected_opacity;
						}
						if (conf.hasOwnProperty('pointer')) {
							selectedMarkText = conf.pointer;
						}
						if (conf.hasOwnProperty('corner')) {
							borderRadius = conf.corner;
						}
						if (conf.hasOwnProperty('font_size')) {
							fontSize = conf.font_size;
						}
					}
				} catch (e:Error) {
				}
			}
			
			measure();
		}
		
		override protected function get requiredTraits():Vector.<String>
		{
			return _requiredTraits;
		}
		
		override protected function processRequiredTraitsAvailable(element:MediaElement):void
		{
			dynamicStream = element.getTrait(MediaTraitType.DYNAMIC_STREAM) as DynamicStreamTrait;
			dynamicStream.addEventListener(DynamicStreamEvent.AUTO_SWITCH_CHANGE, onAutoSwitchChange);
			dynamicStream.addEventListener(DynamicStreamEvent.NUM_DYNAMIC_STREAMS_CHANGE, onNumStreamsChange);
			dynamicStream.addEventListener(DynamicStreamEvent.SWITCHING_CHANGE, onSwitchingChange);
			
			var md:MediaMetadata = element.metadata.getValue(MediaMetadata.ID) as MediaMetadata;
			player = md.mediaPlayer;
			
			if (!menuTextFields) {
				createMenu();
			}
		}
		
		override protected function processRequiredTraitsUnavailable(element:MediaElement):void
		{
			if (dynamicStream) {
				dynamicStream.removeEventListener(DynamicStreamEvent.AUTO_SWITCH_CHANGE, onAutoSwitchChange);
				dynamicStream.removeEventListener(DynamicStreamEvent.NUM_DYNAMIC_STREAMS_CHANGE, onNumStreamsChange);
				dynamicStream.removeEventListener(DynamicStreamEvent.SWITCHING_CHANGE, onSwitchingChange);
				dynamicStream = null;
			}
			menuTextFields = null;
			player = null;
		}
		
		override protected function processMediaElementChange(oldMediaElement:MediaElement):void
		{
		}
		
		protected function onAutoSwitchChange(event:DynamicStreamEvent = null):void
		{
		}
		
		protected function onSwitchingChange(event:DynamicStreamEvent = null):void
		{
			trace("onSwitchingChange: currentIndex=" +  dynamicStream.currentIndex + ", " + dynamicStream.getBitrateForIndex(dynamicStream.currentIndex) + "kbps");
			updateHighlightCurrentStream();
		}
		
		protected function onNumStreamsChange(event:DynamicStreamEvent = null):void
		{
		}
		
		protected function updateSelectedMark():void
		{
			var menuItem:TextField;
			var text:String;
			for (var i:Number = 0; i < menuTextFields.length; i++) {
				menuItem = menuTextFields[i];
				text = menuItem.text;
				if (text.indexOf(selectedMarkText) == 0) {
					menuItem.text = ' ' + text.substr(1);
				}
			}
			if (menuTextFields.length > 0 &&  selectedIndex < menuTextFields.length)
			{
				menuItem = menuTextFields[selectedIndex];
				text = menuItem.text;
				menuItem.text = selectedMarkText + text.substr(1);
			}
		}
		
		protected function updateHighlightCurrentStream():void
		{
			clickArea.graphics.clear();
			clickArea.graphics.beginFill(backgroundColor, backgroundOpacity);
			clickArea.graphics.lineStyle(1, borderColor, borderOpacity);
			clickArea.graphics.drawRoundRect(0, 0, clickArea.width + 20, clickArea.height + 7, borderRadius);
			clickArea.graphics.endFill();
			clickArea.graphics.lineStyle();
			
			if (!dynamicStream)
			{
				return;
			}
			
			var menuIndex:int = finMenuIndexByDynamicStreamIndex(dynamicStream.currentIndex);
			if (menuIndex != -1) {
				clickArea.graphics.beginFill(selectedColor, selectedOpacity);
				clickArea.graphics.drawRect(1, menuTextFields[menuIndex].y + 2, clickArea.width - 4, fontSize + 4);
				clickArea.graphics.endFill();
			}
		}
		
		protected function finMenuIndexByMouseEvent(event:MouseEvent):int
		{
			var mouseY:int = this.mouseY;
			for (var i:Number = 0; i < menuTextFields.length; i++) {
				var textField:TextField = menuTextFields[i];
				if (textField.y <= mouseY && (textField.y + textField.height) >= mouseY) {
					return i;
				}
			}
			return -1;
		}
		
		protected function finMenuIndexByDynamicStreamIndex(streamIndex:int):int
		{
			for (var i:int = 0; i < menuStreamItems.length; i++) {
				var textField:TextField = menuTextFields[i];
				if (menuStreamItems[i].streamIndex == streamIndex) {
					return i;
				}
			}
			return -1;
		}
		
		protected function onMouseOver(event:MouseEvent):void
		{
			event.stopPropagation();
			
			var targetIndex:Number = finMenuIndexByMouseEvent(event);
			
			for (var i:int = 0; i < menuTextFields.length; i++) {
				var textField:TextField = menuTextFields[i];
				if (i != targetIndex) {
					textField.textColor = 0xffffff;
				} else {
					textField.textColor = 0xcdcdcd;
				}
			}
		}
		
		protected function onMouseMove(event:MouseEvent):void
		{
			event.stopPropagation();
			
			var targetIndex:Number = finMenuIndexByMouseEvent(event);
			
			for (var i:int = 0; i < menuTextFields.length; i++) {
				var textField:TextField = menuTextFields[i];
				if (i != targetIndex) {
					textField.textColor = 0xffffff;
				} else {
					textField.textColor = 0xcdcdcd;
				}
			}
		}
		
		protected function onMouseClick(event:MouseEvent):void
		{
			event.stopPropagation();
			
			var targetIndex:Number = finMenuIndexByMouseEvent(event);
			if (targetIndex == -1)
			{
				return;
			}
			
			trace("selected: " +  menuTextFields[targetIndex].text);
			
			if (targetIndex == menuTextFields.length -1) {
				dynamicStream.autoSwitch = true;
			} else {
				dynamicStream.autoSwitch = false;
				var switchingIndex:int = menuStreamItems[targetIndex].streamIndex;
				if (dynamicStream.currentIndex != switchingIndex) {
					dynamicStream.switchTo(switchingIndex);
					if (player.canSeek && player.canSeekTo(player.currentTime)) {
						trace('switch and seek!');
						player.seek(player.currentTime);
					}
				}
			}
			
			selectedIndex = targetIndex;
			updateSelectedMark();
		}
		
		private function createMenu(e:Event = null):void
		{
			var fontAsset:FontAsset = assetManager.getAsset('defaultFont') as FontAsset;
			var format:TextFormat = fontAsset ? fontAsset.format : new TextFormat();

			format.font = StrobePlayerStrings.getString(StrobePlayerStrings.DEFAULT_FONT);
			format.size = fontSize;
			
			var createTextField:Function = function ():TextField
			{
				var lb:TextField = new TextField();
				lb.defaultTextFormat = format;
//				lb.embedFonts = true;
				lb.textColor = textColor;
				lb.autoSize = TextFieldAutoSize.LEFT;
				lb.antiAliasType = AntiAliasType.ADVANCED;
				lb.sharpness = 200;
				lb.thickness = 0;
				
				return lb;
			};
			
			menuStreamItems = new Vector.<Object>;
			menuTextFields = new Vector.<TextField>;
			var mediaMetadata:MediaMetadata = media.metadata.getValue(MediaMetadata.ID) as MediaMetadata;
			if (mediaMetadata) {
				var resourceMetadata:ResourceMetadata = mediaMetadata.resourceMetadata;
				if (resourceMetadata.streamItems) {
					var sItems:Vector.<DynamicStreamingItem> = resourceMetadata.streamItems;
					var yPos:int = 3;
					var mWidth:int = 0;
					
					// Caption
					var lb:TextField = createTextField();
					lb.text = StrobePlayerStrings.getString(StrobePlayerStrings.QUALITY);
					lb.y = yPos;
					lb.x = 7;
					clickArea.addChild(lb);
					
					yPos += lb.height;
					
					// Stream
					for (var i:int = 0; i < sItems.length; i++) {
						var item:Object = {
							streamIndex: i,
							streamName: sItems[i].streamName,
							bitrate: sItems[i].bitrate,
							width: sItems[i].width,
							height: sItems[i].height
						};
						if (dynamicStream) {
							var bitrate2:Number = dynamicStream.getBitrateForIndex(i);
							if (bitrate2 > 0) {
								item.bitrate = bitrate2;
							}
						}
						menuStreamItems.push(item);
					}
					// sort
					function SortMenuStreamItem(a:Object, b:Object):Number{
						return b.bitrate - a.bitrate;
					}
					menuStreamItems.sort(SortMenuStreamItem);
					for each (var menuStreamItem:Object in menuStreamItems){
						lb = createTextField();
						lb.y = yPos;
						lb.x = 7;
						trace("menuStreamItem: " + menuStreamItem.bitrate + "kbps, " + menuStreamItem.height + "px " + convertQualityString(menuStreamItem.bitrate, menuStreamItem.height));
						lb.text = "   " + convertQualityString(menuStreamItem.bitrate, menuStreamItem.height);
						clickArea.addChild(lb);
						menuTextFields.push(lb);
						mWidth = Math.max(mWidth, lb.width + 7);
						yPos += lb.height;
					}
					
					// Auto
					lb = createTextField();
					lb.y = yPos;
					lb.x = 7;
					lb.text = "   " + StrobePlayerStrings.getString(StrobePlayerStrings.QUALITY_AUTO);
					clickArea.addChild(lb);
					menuTextFields.push(lb);
					
					yPos += lb.height;
				}
			}
			measure();
			
			clickArea.graphics.clear();
			clickArea.graphics.beginFill(backgroundColor, backgroundOpacity);
			clickArea.graphics.lineStyle(1, borderColor, borderOpacity);
			clickArea.graphics.drawRoundRect(0, 0, mWidth + 25, yPos + 5, borderRadius);
			clickArea.graphics.endFill();
			clickArea.graphics.lineStyle();
			
			this.height += 5;
			
			if (menuTextFields.length > 0)
			{
				// last index is "Auto"
				selectedIndex = menuTextFields.length -1;
				updateSelectedMark();
			}
		}
		
		private static function convertQualityString(kBitRate:Number, videoHeight:Number):String
		{
			var quality:String = QualitySwitcher.convertQuality(kBitRate, videoHeight);
			return StrobePlayerStrings.getString(quality);
		}
		
		public static var menuConfig:String = null;
		
		private var dynamicStream:DynamicStreamTrait;
		private var player:MediaPlayer;
		
		private var menuStreamItems:Vector.<Object> = new Vector.<Object>;
		private var menuTextFields:Vector.<TextField> = new Vector.<TextField>;
		private var clickArea:Sprite;
		
		private var selectedMarkText:String = "✔";
		private var selectedIndex:int = -1;
		
		private var textColor:uint = 0xcdcdcd;
		private var backgroundColor:uint = 0x0;
		private var backgroundOpacity:Number = 0.5;
		private var selectedColor:uint = 0x5c5c5c;
		private var selectedOpacity:Number = 0.8;
		private var borderColor:uint = 0xffffff;
		private var borderOpacity:Number = 0.7;
		private var borderRadius:Number = 3;
		private var fontSize:int = 12;
		
		private static const _requiredTraits:Vector.<String> = new Vector.<String>;
		_requiredTraits[0] = MediaTraitType.DYNAMIC_STREAM;
	}
}