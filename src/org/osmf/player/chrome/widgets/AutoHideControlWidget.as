package org.osmf.player.chrome.widgets
{
	import flash.display.StageDisplayState;
	import flash.events.Event;

	public class AutoHideControlWidget extends AutoHideWidget
	{
		public function AutoHideControlWidget()
		{
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			super();
		}
		
		private function onAddedToStage(event:Event):void
		{
			if (stage != null)
			{
				stage.addEventListener(Event.MOUSE_LEAVE, onStageMouseLeave);
			}
		}
		
		private function onStageMouseLeave(event:Event):void
		{
			if (stage != null)
			{
				if (stage.displayState == StageDisplayState.NORMAL) {
					visible = false;
				}
			}
		}
	}
}