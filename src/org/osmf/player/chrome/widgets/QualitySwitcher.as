package org.osmf.player.chrome.widgets
{
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import org.osmf.events.DisplayObjectEvent;
	import org.osmf.events.DynamicStreamEvent;
	import org.osmf.layout.HorizontalAlign;
	import org.osmf.layout.LayoutMode;
	import org.osmf.media.MediaElement;
	import org.osmf.player.chrome.assets.AssetIDs;
	import org.osmf.player.chrome.assets.AssetsManager;
	import org.osmf.player.chrome.hint.WidgetHint;
	import org.osmf.player.metadata.MediaMetadata;
	import org.osmf.player.metadata.ResourceMetadata;
	import org.osmf.player.utils.StrobePlayerStrings;
	import org.osmf.traits.DisplayObjectTrait;
	import org.osmf.traits.DynamicStreamTrait;
	import org.osmf.traits.MediaTraitType;

	public class QualitySwitcher extends Widget
	{
		public function QualitySwitcher()
		{
			super();
			
			mouseEnabled = true;
			
			addEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
			addEventListener(MouseEvent.MOUSE_OUT, onMouseOut);
			addEventListener(MouseEvent.CLICK, onMouseClick);
		}
		
		override public function configure(xml:XML, assetManager:AssetsManager):void
		{
			layoutMetadata.percentHeight = 100;
			
			face = AssetIDs.QUALITY_AUTO;
			
			faceAuto = assetManager.getDisplayObject(AssetIDs.QUALITY_AUTO);
			faceHdHigh = assetManager.getDisplayObject(AssetIDs.QUALITY_HD_HIGH);
			faceHdLow = assetManager.getDisplayObject(AssetIDs.QUALITY_HD_LOW);
			faceSdHigh = assetManager.getDisplayObject(AssetIDs.QUALITY_SD_HIGH);
			faceSdLow = assetManager.getDisplayObject(AssetIDs.QUALITY_SD_LOW);
			
			menu = new QualityMenu();
			menu.configure(xml, assetManager);
			menu.layoutMetadata.layoutMode = LayoutMode.VERTICAL;

			super.configure(xml, assetManager);
		}
		
		override protected function get requiredTraits():Vector.<String>
		{
			return _requiredTraits;
		}
		
		override protected function processMediaElementChange(oldElement:MediaElement):void
		{
			visibilityDeterminingEventHandler();
		}
		
		override protected function processRequiredTraitsAvailable(element:MediaElement):void
		{
			dynamicStream = element.getTrait(MediaTraitType.DYNAMIC_STREAM) as DynamicStreamTrait;
			dynamicStream.addEventListener(DynamicStreamEvent.AUTO_SWITCH_CHANGE, onAutoSwitchChange);
			dynamicStream.addEventListener(DynamicStreamEvent.NUM_DYNAMIC_STREAMS_CHANGE, onNumStreamsChange);
			dynamicStream.addEventListener(DynamicStreamEvent.SWITCHING_CHANGE, onSwitchingChange);
			
			displayObjectTrait = element.getTrait(MediaTraitType.DISPLAY_OBJECT) as DisplayObjectTrait;
			displayObjectTrait.addEventListener(DisplayObjectEvent.MEDIA_SIZE_CHANGE, onMediaSizeChange);
			
			visibilityDeterminingEventHandler();
		}
		
		override protected function processRequiredTraitsUnavailable(element:MediaElement):void
		{
			WidgetHint.getInstance(this).hide();
			if (dynamicStream)
			{
				dynamicStream.removeEventListener(DynamicStreamEvent.AUTO_SWITCH_CHANGE, onAutoSwitchChange);
				dynamicStream.removeEventListener(DynamicStreamEvent.NUM_DYNAMIC_STREAMS_CHANGE, onNumStreamsChange);
				dynamicStream.removeEventListener(DynamicStreamEvent.SWITCHING_CHANGE, onSwitchingChange);
				dynamicStream = null;
			}
			if (displayObjectTrait)
			{
				displayObjectTrait.removeEventListener(DisplayObjectEvent.MEDIA_SIZE_CHANGE, onMediaSizeChange);
				displayObjectTrait = null;
			}
			visibilityDeterminingEventHandler();
		}
		
		public static function convertQuality(kBitRate:Number, videoHeight:Number):String
		{
			if (kBitRate >= 5000) {
				return StrobePlayerStrings.QUALITY_HIGH_HD;
			} else if (kBitRate >= 3500) {
				return StrobePlayerStrings.QUALITY_HD;
			} else if (kBitRate >= 1000) {
				return StrobePlayerStrings.QUALITY_HIGH_SD;
			} else {
				return StrobePlayerStrings.QUALITY_SD;
			}
		}
		
		// Internals
		//
		private function onMediaSizeChange(event:DisplayObjectEvent):void
		{
			if (visible)
			{
				updateFace();
			}
		}
		
		private function onAutoSwitchChange(event:DynamicStreamEvent = null):void
		{
			visibilityDeterminingEventHandler();
		}
		
		private function onNumStreamsChange(event:DynamicStreamEvent = null):void
		{
			visibilityDeterminingEventHandler();
		}
		
		private function onSwitchingChange(event:DynamicStreamEvent = null):void
		{
			visibilityDeterminingEventHandler();
		}
		
		private function updateFace():void
		{
			if (media != null && dynamicStream != null)
			{
				var face:DisplayObject = null;
				
				if (dynamicStream.autoSwitch)
				{
					face = faceAuto;
				}
				else
				{
					// Try to use the height from the resource metadata. 
					// Note that OSMF doesn't propagate the w/h of dynamic streams in DynamicStreamTrait.
					var mediaMetadata:MediaMetadata = media.metadata.getValue(MediaMetadata.ID) as MediaMetadata;
					if (mediaMetadata)
					{
						var resourceMetadata:ResourceMetadata = mediaMetadata.resourceMetadata;
						if (resourceMetadata.streamItems)
						{
							var quality:String = QualitySwitcher.convertQuality(
								dynamicStream.getBitrateForIndex(dynamicStream.currentIndex),
								resourceMetadata.streamItems[dynamicStream.currentIndex].height
							);
							if (quality == StrobePlayerStrings.QUALITY_HIGH_HD)
							{
								face = faceHdHigh;
							}
							else if (quality == StrobePlayerStrings.QUALITY_HD)
							{
								face =  faceHdLow;
							}
							else if (quality == StrobePlayerStrings.QUALITY_HIGH_SD)
							{
								face =  faceSdHigh;
							}
							else if (quality == StrobePlayerStrings.QUALITY_SD)
							{
								face =  faceSdLow;
							}
						}
					}
				}
				
				if (numChildren > 0 && getChildAt(0) != face)
				{
					removeChildAt(0);
				}
				
				if (face && contains(face) == false)
				{
					addChildAt(face, 0);
				}
				
				width = face ? face.width + (layoutMetadata.paddingRight || 0) : 0;
				height = face ? face.height : 0;
				
				measure();
			}
		}
		
		private function visibilityDeterminingEventHandler():void
		{
			visible = media != null && dynamicStream != null;
			if (visible)
			{
				updateFace();
			}
		}
		
		override public function set media(value:MediaElement):void
		{
			if (value != null)
			{
				super.media = value;
				menu.media = value;
			}
		}
		
		override public function layout(availableWidth:Number, availableHeight:Number, deep:Boolean=true):void
		{
			var hintWidget:Widget = WidgetHint.getInstance(this).widget;
			WidgetHint.getInstance(this).hide();
			measure();
			super.layout(Math.max(measuredWidth, availableWidth), Math.max(measuredHeight, availableHeight));
			WidgetHint.getInstance(this).widget = hintWidget;
		}
		
		protected function onMouseOver(event:MouseEvent):void
		{
			WidgetHint.getInstance(this).horizontalAlign = HorizontalAlign.CENTER;
			if (menu) {
				WidgetHint.getInstance(this).widget = menu;
			}
			if (event.localY < 0)
			{
				menu.dispatchEvent(event.clone());
			}
		}
		
		protected function onMouseOut(event:MouseEvent):void
		{
			WidgetHint.getInstance(this).hide();
		}
		
		protected function onMouseClick(event:MouseEvent):void
		{
			if (event.localY < 0)
			{
				menu.dispatchEvent(event.clone());
				return;
			}
		}
		
		private var menu:QualityMenu;
		private var dynamicStream:DynamicStreamTrait;
		private var displayObjectTrait:DisplayObjectTrait;
		
		private var faceAuto:DisplayObject;
		private var faceHdHigh:DisplayObject;
		private var faceHdLow:DisplayObject;
		private var faceSdHigh:DisplayObject;
		private var faceSdLow:DisplayObject;
				
//		private var hdOn:DisplayObject;
//		private var hdOff:DisplayObject;
		
		/* static */
		private static const _requiredTraits:Vector.<String> = new Vector.<String>;
		_requiredTraits[0] = MediaTraitType.DYNAMIC_STREAM;
		_requiredTraits[1] = MediaTraitType.DISPLAY_OBJECT;
	}
}