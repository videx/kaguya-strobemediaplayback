package org.osmf.player.chrome.widgets
{
	import org.osmf.player.chrome.assets.AssetIDs;
	import org.osmf.player.chrome.assets.AssetsManager;
		
	public class WindowCloseButton extends ButtonWidget
	{
		public function WindowCloseButton()
		{
			super();
			
			upFace = AssetIDs.CLOSE_BUTTON_NORMAL;
			downFace = AssetIDs.CLOSE_BUTTON_DOWN;
			overFace = AssetIDs.CLOSE_BUTTON_OVER;
			
			lastWidth = 0;
			lastHeight = 0;
		}
		
		override public function configure(xml:XML, assetManager:AssetsManager):void
		{
			super.configure(xml, assetManager);
			visible = false;
		}
		
		override public function layout(availableWidth:Number, availableHeight:Number, deep:Boolean=true):void
		{
			super.layout(availableWidth, availableHeight, deep);
			
			if (lastWidth != availableWidth && lastHeight != availableHeight) {
				this.graphics.clear();
				this.graphics.beginFill(0x000000, 0.6);
				this.graphics.drawRect(0, 0, width, height);
				this.graphics.endFill();
				lastWidth = availableWidth;
				lastHeight = availableHeight;
			}
		}
		
		private var lastWidth:Number;
		private var lastHeight:Number;
	}
}
