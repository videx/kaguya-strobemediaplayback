package org.osmf.player.chrome.widgets
{
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Rectangle;
	import flash.net.URLRequest;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;

	import org.osmf.player.chrome.assets.AssetsManager;
	import org.osmf.player.chrome.utils.FormatUtils;
	
	public class ScrubBarThumbnailHintWidget extends Widget
	{
		protected static var _spriteThumbnails:Array;
		
		public static function setSpriteThumbnails(spriteThumbnails:Array):void
		{
			if (spriteThumbnails == null) {
				_spriteThumbnails = [];
			} else {
				_spriteThumbnails = spriteThumbnails;
			}
		}
		
		public function ScrubBarThumbnailHintWidget()
		{
			super();
		}

		override public function configure(xml:XML, assetManager:AssetsManager):void
		{
			super.configure(xml, assetManager);
			
			_timeBar = new Sprite();
			_timeBar.graphics.beginFill(0x000000, 0);
			_timeBar.graphics.drawRect(0, 0, 74, 18);
			_timeBar.graphics.endFill();
			var vertices:Vector.<Number> = new Vector.<Number>();
			vertices.push(32, 18, 42, 18, 37, 23);
			_timeBar.graphics.beginFill(backgroundColor, backgroundOpacity);
			_timeBar.graphics.drawTriangles(vertices);
			_timeBar.graphics.endFill();

			_timeLabel = new TextField();
			var timeTextFormat:TextFormat = new TextFormat();
			timeTextFormat.size = 11;
			timeTextFormat.font = "Arial";
			timeTextFormat.align = TextFieldAutoSize.CENTER;
			_timeLabel.width = _timeBar.width;
			_timeLabel.height = 15;
			_timeLabel.textColor = 0xffffff;
			_timeLabel.defaultTextFormat = timeTextFormat;

			_timeBar.addChild(_timeLabel);
			_timeLabel.x = 0;
			_timeLabel.y = 0;

			addChild(_timeBar);
			_timeBar.x = 0;
		}

		public function set currentTime(time:Number):void
		{
			_timeLabel.text = FormatUtils.formatTimeStatus(time, Number.NaN)[0];
			if (_thumbnailContainer) {
				var total_time:Number = 0;
				var i:int = 0;
				for (; i < _thumbnailLoaderList.length; i++) {
					_thumbnailLoaderList[i].loader.visible = false;
				}
				time += 60;
				for (i = 0; i < _thumbnailLoaderList.length; i++) {
					if (time * 1000 < -(-total_time - _thumbnailLoaderList[i].streamlength)) {
						_thumbnailLoaderList[i].loader.visible = true;
						var diff:int = (time - total_time / 1000) % 60;
						diff = diff == 0 ? 1 : 0;
						var pos_x:int = ((time - total_time / 1000) / 60 - diff) % 10;
						var pos_y:int = ((time - total_time / 1000) / 60) / 10;
						_thumbnailLoaderList[i].loader.x = -pos_x * _thumnailWidth;
						_thumbnailLoaderList[i].loader.y = -pos_y * _thumnailHeight;
						break;
					}
					total_time = -(-total_time - _thumbnailLoaderList[i].streamlength);
				}
			}
		}
		
		public function loadThumbnailList():void
		{
			setThumbnailList(_spriteThumbnails);
		}
		private function setThumbnailList(thumbnails:Array):void
		{
			_thumbnailLoaderList = [];
			if (_thumbnailContainer) {
				removeChild(_thumbnailContainer);
				_thumbnailContainer = null;
			}
			
			if (thumbnails != null && thumbnails.length > 0)
			{
				for (var i:int; i < thumbnails.length; i++) {
					var filepath:String = thumbnails[i].url;
					var thumloader:Object = {loader: new Loader(), streamlength: thumbnails[i].time};
					_thumbnailLoaderList.push(thumloader);
					thumloader.loader.load(new URLRequest(filepath));
					thumloader.loader.contentLoaderInfo.addEventListener(Event.COMPLETE, function (e):void {
						if (!_thumbnailContainer) {
							_thumnailWidth = e.target.loader.width / 10;
							_thumbnailContainer = new Sprite();
							addChildAt(_thumbnailContainer, 0);
							_thumbnailContainer.scrollRect = new Rectangle(0, 0, _thumnailWidth, _thumnailHeight);
							_thumbnailContainer.x = 2;
							_thumbnailContainer.y = 2;
							graphics.clear();
							graphics.beginFill(backgroundColor, backgroundOpacity);
							graphics.drawRoundRect(0, 0, _thumnailWidth + 4, _thumnailHeight + 20, borderRadius);
							graphics.endFill();
							_timeBar.x = (_thumnailWidth + 4) / 2 - _timeBar.width / 2;
							_timeBar.y = _thumnailHeight + 2;
						}
						e.target.loader.visible = false;
						_thumbnailContainer.addChild(e.target.loader as Loader);
					});
				}
			} else {
				graphics.clear();
				graphics.beginFill(backgroundColor, backgroundOpacity);
				graphics.drawRoundRect(0, 0, 74, 18, borderRadius);
				graphics.endFill();
				_timeBar.x = 0;
				_timeBar.y = 0;
			}
		}
		
		private var backgroundColor:uint = 0x0;
		private var backgroundOpacity:Number = 1;
		private var borderRadius:Number = 3;
		
		private var _timeBar:Sprite;
		private var _timeLabel:TextField;
		
		private var _thumnailWidth:Number;
		private var _thumnailHeight:Number = 60;
		private var _thumnailLoader:Loader = null;
		
		private var _thumnailSprite:Sprite = null;
		
		private var _thumbnailContainer:Sprite = null;
		private var _thumbnailLoaderList:Array = null;
	}
}