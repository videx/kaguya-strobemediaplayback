/***********************************************************
 * Copyright 2010 Adobe Systems Incorporated.  All Rights Reserved.
 *
 * *********************************************************
 * The contents of this file are subject to the Berkeley Software Distribution (BSD) Licence
 * (the "License"); you may not use this file except in
 * compliance with the License. 
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations
 * under the License.
 *
 *
 * The Initial Developer of the Original Code is Adobe Systems Incorporated.
 * Portions created by Adobe Systems Incorporated are Copyright (C) 2010 Adobe Systems
 * Incorporated. All Rights Reserved.
 * 
 **********************************************************/

package org.osmf.player.chrome
{
	import org.osmf.layout.HorizontalAlign;
	import org.osmf.layout.LayoutMode;
	import org.osmf.layout.VerticalAlign;
	import org.osmf.player.chrome.assets.AssetsManager;
	import org.osmf.player.chrome.widgets.AutoHideControlWidget;
	import org.osmf.player.chrome.widgets.FullScreenEnterButton;
	import org.osmf.player.chrome.widgets.FullScreenLeaveButton;
	import org.osmf.player.chrome.widgets.MuteButton;
	import org.osmf.player.chrome.widgets.PauseButton;
	import org.osmf.player.chrome.widgets.PlayButton;
	import org.osmf.player.chrome.widgets.PlaylistNextButton;
	import org.osmf.player.chrome.widgets.PlaylistPreviousButton;
	import org.osmf.player.chrome.widgets.QualitySwitcher;
	import org.osmf.player.chrome.widgets.ScrubBar;
	import org.osmf.player.chrome.widgets.TimeViewWidget;
	import org.osmf.player.chrome.widgets.Widget;
	import org.osmf.player.chrome.widgets.WidgetIDs;

	/**
	 * ControlBar contains all the control widgets and is responsible for their layout.
	 */ 
	public class ControlBar extends AutoHideControlWidget implements IControlBar
	{
		// Overrides
		//
		override public function configure(xml:XML, assetManager:AssetsManager):void
		{
			id = WidgetIDs.CONTROL_BAR;
			fadeSteps = 6;
			
			layoutMetadata.horizontalAlign = HorizontalAlign.LEFT;
			layoutMetadata.verticalAlign = VerticalAlign.TOP;
			layoutMetadata.layoutMode = LayoutMode.VERTICAL;
			super.configure(xml, assetManager);
			
			var row1:Widget = new Widget();
			row1.layoutMetadata.horizontalAlign = HorizontalAlign.LEFT;
			row1.layoutMetadata.verticalAlign = VerticalAlign.TOP;
			row1.layoutMetadata.layoutMode = LayoutMode.HORIZONTAL;
			row1.layoutMetadata.percentWidth = 100;
			row1.layoutMetadata.height = 9;
			addChildWidget(row1);
			var row2:Widget = new Widget();
			row2.layoutMetadata.horizontalAlign = HorizontalAlign.LEFT;
			row2.layoutMetadata.verticalAlign = VerticalAlign.TOP;
			row2.layoutMetadata.layoutMode = LayoutMode.HORIZONTAL;
			row2.layoutMetadata.percentWidth = 100;
			row2.layoutMetadata.height = 27;
			addChildWidget(row2);
			
			// padding
			var leftPaddingSpacer:Widget = new Widget();
			leftPaddingSpacer.width = 5;
			leftPaddingSpacer.layoutMetadata.horizontalAlign = HorizontalAlign.LEFT;
			row2.addChildWidget(leftPaddingSpacer);
			
			var leftControls:Widget = new Widget();
			leftControls.layoutMetadata.percentHeight = 100;
			leftControls.layoutMetadata.layoutMode = LayoutMode.HORIZONTAL;
			leftControls.layoutMetadata.horizontalAlign = HorizontalAlign.LEFT;
			
			// Play/pause
			var playButton:PlayButton = new PlayButton();
			playButton.layoutMetadata.verticalAlign = VerticalAlign.MIDDLE
			playButton.layoutMetadata.horizontalAlign = HorizontalAlign.LEFT;
			playButton.player = this.player;
			leftControls.addChildWidget(playButton);
			
			var pauseButton:PauseButton = new PauseButton();
			pauseButton.layoutMetadata.verticalAlign = VerticalAlign.MIDDLE
			pauseButton.layoutMetadata.horizontalAlign = HorizontalAlign.LEFT;
			pauseButton.player = this.player;
			leftControls.addChildWidget(pauseButton);
			
			// Previous/Next
			var previousButton:PlaylistPreviousButton = new PlaylistPreviousButton();
			previousButton.layoutMetadata.verticalAlign = VerticalAlign.MIDDLE
			previousButton.layoutMetadata.horizontalAlign = HorizontalAlign.LEFT;
			previousButton.player = this.player;
			leftControls.addChildWidget(previousButton);
			
			var nextButton:PlaylistNextButton = new PlaylistNextButton();
			nextButton.layoutMetadata.verticalAlign = VerticalAlign.MIDDLE
			nextButton.layoutMetadata.horizontalAlign = HorizontalAlign.LEFT;
			nextButton.player = this.player;
			leftControls.addChildWidget(nextButton);
			
			// Mute/unmute
			var muteButton:MuteButton = new MuteButton();
			muteButton.id = WidgetIDs.MUTE_BUTTON;
			muteButton.volumeSteps = 3;
			muteButton.layoutMetadata.verticalAlign = VerticalAlign.MIDDLE;
			muteButton.layoutMetadata.horizontalAlign = HorizontalAlign.LEFT;
			muteButton.player = this.player;
			leftControls.addChildWidget(muteButton);
			
			// Spacer
			var afterVolumeSpacer:Widget = new Widget();
			afterVolumeSpacer.width = 5;
			leftControls.addChildWidget(afterVolumeSpacer);
			
			// Time view
			var timeViewWidget:TimeViewWidget = new TimeViewWidget();
			timeViewWidget.layoutMetadata.verticalAlign = VerticalAlign.MIDDLE;
			timeViewWidget.layoutMetadata.horizontalAlign = HorizontalAlign.LEFT;
			leftControls.addChildWidget(timeViewWidget);
			
			row2.addChildWidget(leftControls);
			
			var spacer:Widget = new Widget();
			spacer.layoutMetadata.horizontalAlign = HorizontalAlign.CENTER;
			spacer.layoutMetadata.verticalAlign = VerticalAlign.MIDDLE;
			spacer.layoutMetadata.percentWidth = 100;
			row2.addChildWidget(spacer);
			
			// Spacer
			var beforeScrubSpacer:Widget = new Widget();
			beforeScrubSpacer.layoutMetadata.horizontalAlign = HorizontalAlign.LEFT;
			beforeScrubSpacer.width = 10;
			row1.addChildWidget(beforeScrubSpacer);
			
			// Scrub bar
			scrubBar = new ScrubBar();
			scrubBar.id = WidgetIDs.SCRUB_BAR;
			scrubBar.layoutMetadata.horizontalAlign = HorizontalAlign.LEFT;
			scrubBar.layoutMetadata.verticalAlign = VerticalAlign.MIDDLE;
			scrubBar.layoutMetadata.percentWidth = 100;
			scrubBar.player = this.player;
			row1.addChildWidget(scrubBar);
			
			// Spacer
			var afterScrubSpacer:Widget = new Widget();
			afterScrubSpacer.layoutMetadata.horizontalAlign = HorizontalAlign.LEFT;
			afterScrubSpacer.width = 10;
			row1.addChildWidget(afterScrubSpacer);
			
			// Right side
			var rightControls:Widget = new Widget();
			rightControls.layoutMetadata.percentHeight = 100;
			rightControls.layoutMetadata.layoutMode = LayoutMode.HORIZONTAL;
			rightControls.layoutMetadata.horizontalAlign = HorizontalAlign.RIGHT;
			rightControls.layoutMetadata.verticalAlign = VerticalAlign.MIDDLE;
			
			// HD indicator
			var qualitySwitcher:QualitySwitcher = new QualitySwitcher();
			qualitySwitcher.layoutMetadata.verticalAlign = VerticalAlign.MIDDLE;
			qualitySwitcher.layoutMetadata.horizontalAlign = HorizontalAlign.RIGHT;
			rightControls.addChildWidget(qualitySwitcher);
			
			// FullScreen
			var fullscreenLeaveButton:FullScreenLeaveButton = new FullScreenLeaveButton();
			fullscreenLeaveButton.layoutMetadata.verticalAlign = VerticalAlign.MIDDLE;
			fullscreenLeaveButton.layoutMetadata.horizontalAlign = HorizontalAlign.RIGHT;
			rightControls.addChildWidget(fullscreenLeaveButton);
			var fullscreenEnterButton:FullScreenEnterButton = new FullScreenEnterButton();
			fullscreenEnterButton.id = WidgetIDs.FULL_SCREEN_ENTER_BUTTON; 
			fullscreenEnterButton.layoutMetadata.verticalAlign = VerticalAlign.MIDDLE;
			fullscreenEnterButton.layoutMetadata.horizontalAlign = HorizontalAlign.RIGHT;
			rightControls.addChildWidget(fullscreenEnterButton);
			
			row2.addChildWidget(rightControls);
			
			// padding
			var rightPaddingSpacer:Widget = new Widget();
			rightPaddingSpacer.width = 5;
			rightPaddingSpacer.layoutMetadata.horizontalAlign = HorizontalAlign.RIGHT;
			row2.addChildWidget(rightPaddingSpacer);
			
			configureWidgets
				(	[ pauseButton, playButton, previousButton, nextButton
					, muteButton, afterVolumeSpacer
					, timeViewWidget
					, leftControls, leftPaddingSpacer
					, beforeScrubSpacer, scrubBar, afterScrubSpacer
					, qualitySwitcher
					, fullscreenEnterButton, fullscreenLeaveButton
					, rightControls, rightPaddingSpacer
					, row1, row2
					]
				);
			
			measure();
		}
		
		override public function layout(availableWidth:Number, availableHeight:Number, deep:Boolean=true):void
		{
			super.layout(availableWidth, availableHeight, deep);
			drawBackGround();
		}
		
		override protected function isKeepVisibleAgainstAutoHide():Boolean
		{
			if (scrubBar && scrubBar.sliding) {
				return true;
			}
			return super.isKeepVisibleAgainstAutoHide();
		}

		// Internals
		//
		
		private function drawBackGround():void
		{
			var scrubberPaddingY:int = 2;
			this.graphics.clear();
			this.graphics.beginFill(0x000000, 0.6);
			this.graphics.drawRect(0, scrubberPaddingY, width, height - 2);
			this.graphics.endFill();
		}
	
		private function configureWidgets(widgets:Array):void
		{
			for each( var widget:Widget in widgets)
			{
				if (widget)
				{
					widget.configure(<default/>, assetManager);
				}
			}
		}
		
		
		private var scrubBar:ScrubBar;
	}
}